import threading
import time
import yaml
from subprocess import (
    CalledProcessError,
    PIPE,
    Popen,
    check_call,
)


class Job(object):
    """
    Job
    """
    def __init__(self,
                 job_name=None,
                 command=None,
                 alert_command=None,
                 loop_time=None):
        """
        """
        self.job_name = job_name
        self.command = command
        self.alert_command = alert_command
        self.loop_time = loop_time


class NetMon(object):
    """
    """
    def __init__(self, debug=False):
        self.debug = debug
        self.jobs = self.__load_jobs()
        self.job_threads = self.__setup_threads()
        self.running_jobs = list()

    def __load_jobs(self):
        config = self.__load_config()
        __jobs = config['jobs']
        jobs = list()

        for job in __jobs:
            j = Job(job,
                    __jobs[job]['command'],
                    __jobs[job]['alert_command'],
                    __jobs[job]['loop_time'])
            jobs.append(j)
        return jobs

    def __load_config(self):
        with open('./netmon_config.yaml', 'r') as f:
            return yaml.load(f.read(),
                             Loader=yaml.SafeLoader)

    def __setup_threads(self):
        job_threads = list()
        for job in self.jobs:
            job_t = threading.Thread(target=self.__run,
                                     args=(job,))
            job_threads.append(job_t)
        return job_threads

    def __run(self, job):
        command = job.command
        alert_command = job.alert_command
        loop_time = job.loop_time

        try:
            check_call(args=command.split(','),
                       stdout=PIPE,
                       stderr=PIPE)
        except CalledProcessError:
            Popen(args=alert_command.split(','))
        time.sleep(loop_time)
        self.running_jobs.remove(job.job_name)

    def run(self):
        while True:
            for thread in self.job_threads:
                thread_job_name = thread._args[0].job_name
                if thread_job_name not in self.running_jobs:
                    thread.start()
                    self.running_jobs.append(thread_job_name)
                else:
                    continue
            self.job_threads = self.__setup_threads()


if __name__ == '__main__':
    mon = NetMon()
    mon.run()
